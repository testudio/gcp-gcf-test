# GCP GCF GitLab CI test project
This test project demonstrates how to develop Google Cloud function locally and deploy it to GCP from local dev environment and from GitLab CI.

## Requirements

- install Node v8 (v10 is in Beta as of 03/06/2019)
- install GCP SDK 

  for example, on MacOS run

  `brew cask install google-cloud-sdk`

- enable GCP billing, enable Cloud Functions API
- create GCP service account with a role of `Cloud Functions Developer` and `Service Account User`, generate key and private key json. Login as service account:

  `gcloud auth activate-service-account --key-file ./private-key-12345.json`

  *DO NOT COMMIT PRIVATE KEY*

## Development

Install modules by running:

```console
$ npm install
```

## Testing

Unit/integration tests are running with AVA test framework, using sinon to mock funcations and nock to mock HTTP responses

Run tests by running: 

```console
$ npm test
```

### Debugging tests with Chrome DevTools

Use [inspect-process](https://github.com/jaridmargolin/inspect-process) to easily launch a debugging session with Chrome DevTools.


```console
$ npm install --global inspect-process
```

```console
$ inspect node_modules/ava/profile.js tests/file.test.js
```

## Deployment

## Local

Deploy function by running: 

- `gcloud functions deploy [gcf-name] --runtime [nodejs8] --trigger-http --project [gcp-project-id] --memory [XXXMB]`

## GitLab CI

1. Create variables in GitLab: https://gitlab.com/testudio/gcp-gcf-test/settings/ci_cd

- `GCP_SERVICE_ACCOUNT_KEY` - copy contents of service account private key, set TYPE to file
- `GCP_PROJECT_ID` - Google project id, set TYPE to variable)
- any other configuration values for deployment script, eg memory, runtime, region, etc

2. GitLab CI will run if repo contains .gitlab-ci.yml file which defines:
- container image with cloud-sdk: https://hub.docker.com/r/google/cloud-sdk
- scripts to run at deployment, eg authenticate and deploy function

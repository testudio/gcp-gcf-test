/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

exports.gitlabCiTest = (req, res) => {
  const isKeyMatch = req.query.key === process.env.API_KEY;

  if (req.method !== 'GET' || !isKeyMatch) {
    res.status(500).send({ error: 'Unsupported api call' });
  } else {
    const message = req.query.message || 'Sample function deployed from GitLab CI';
    res.status(200).send(message);
  }
};

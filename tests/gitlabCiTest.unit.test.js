import test from 'ava';
import sinon from 'sinon';
import nock from 'nock';

// file to test
import { gitlabCiTest } from '../index';

// disable internet
nock.disableNetConnect();

const defaultEnv = {
  API_KEY: 'ABC123',
};

test('returns error response if method is not GET', async (t) => {
  const mockRequest = {
    method: 'POST',
    query: {
      key: 'ABC',
    },
  };

  const mockResponse = {
    status: sinon.stub().returnsThis(),
    send: sinon.stub(),
  };

  gitlabCiTest(mockRequest, mockResponse);

  const responseStatus = mockResponse.status.firstCall.args[0];
  t.is(responseStatus, 500);
});

test('returns error response if key doesn\'t match', async (t) => {
  const mockRequest = {
    method: 'GET',
    query: {
      key: 'INVALID KEY',
    },
  };

  const mockResponse = {
    status: sinon.stub().returnsThis(),
    send: sinon.stub(),
  };

  await gitlabCiTest(mockRequest, mockResponse);
  const responseStatus = mockResponse.status.firstCall.args[0];
  t.is(responseStatus, 500);
});

test('returns valid response', async (t) => {
  // reset env variables
  process.env = { ...process.env, ...defaultEnv };

  const mockRequest = {
    method: 'GET',
    query: {
      key: defaultEnv.API_KEY,
    },
  };

  const mockResponse = {
    status: sinon.stub().returnsThis(),
    send: sinon.stub(),
  };

  gitlabCiTest(mockRequest, mockResponse);
  const responseStatus = mockResponse.status.firstCall.args[0];
  t.is(responseStatus, 200);
});
